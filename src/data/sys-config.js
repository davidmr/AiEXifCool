/**
 * @author Ian
 * @created 2017-2-25 21:48:21
 */
const sysConfig = {
  appName: 'AiEXifCool',
  version: '1.0.0',
  homepage: 'https://github.com/LabsRS-Dev/AiEXifCool'
};

// export
export default { sysConfig };
